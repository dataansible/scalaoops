package scalaJDBC
import java.sql.DriverManager
import java.sql.Connection
object Jdbc extends App{
val driver = "com.mysql.jdbc.Driver"
    val url = "jdbc:mysql://localhost:3306/datadotz"
    val username = "root"
    val password = "root"

    // there's probably a better way to do this
    var connection:Connection = null

    try {
      // make the connection
      Class.forName(driver)
      connection = DriverManager.getConnection(url, username, password)

      // create the statement, and run the select query
      val statement = connection.createStatement()
      val resultSet = statement.executeQuery("SELECT name,item from crud")
      while ( resultSet.next() ) {
        val name = resultSet.getString("name")
        val item = resultSet.getString("item")
        println("Name = " + name )
        println("Item = " + item )
        println()
      }
    } catch {
      case e => e.printStackTrace
    }
    connection.close()
  
  

}