package collectionWorkout

object SampleList {
def main(args: Array[String]): Unit = {
  //Types of List creation
  
  var lang=List("java","scala","JSP",1,1)
  
  var l:List[String]=List("apple","orange","banana")
  
  val version:List[Int]=List(12,32,65,68)
  
  val e: List[Nothing]=List()
  
  //Basic operation in List
  println("Head of the list: " +lang.head)
  println("Last element of list: " +l.last)
  println("Tail of the list: " +l.tail)
  println("To check list is empty or not: " +e.isEmpty)
  
  
}
}