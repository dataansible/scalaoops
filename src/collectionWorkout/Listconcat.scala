package collectionWorkout

object Listconcat {
def main(args: Array[String]): Unit = {
  val a:List[String]=List("apple","orange","pineapple")
  val b:List[String]=List("apple","berry","banana")
  
  var fruit=List.concat(a,b)
  println("To concat of list:" +fruit)
  
  println("To find the distinct element in the List " +fruit.distinct)
  
  println("To flatten the list " +a.flatten)
}
}