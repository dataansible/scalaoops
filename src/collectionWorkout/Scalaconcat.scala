package collectionWorkout

object Scalaconcat {
def main(args: Array[String]): Unit = {
  val a:Set[String]=Set("apple","orange","pineapple","apple")
  val b:Set[String]=Set("berry","apple")
  
  val s=a ++ b
  println("To concat the two set"+s)
  
  val c:Set[Int]=Set(1,8,9,6,2,3)
  println("To find the minimum value: "+c.min)
  println("To find the maximum value: "+c.max)
  
  println("To find the common values: "+a.intersect(b))
}
}