package collectionWorkout

object Option {
def main(args: Array[String]): Unit = {
  var d=Map(1->"India",2->"USA",3->"Russia")
  println("Country="+display(d.get(1)))
  println("Country="+display(d.get(4)))
}
def display(x:Option[String]) = x match{
  case Some(s)=>s
  case None=>"?"
}
}