package collectionWorkout
import scala.collection.mutable.ListBuffer
object Listbuffer {
def main(args: Array[String]): Unit = {
  val a=new ListBuffer[String]() //for string datatype
  a+="java"
  a+="scala"
  a+="c++"
  a+="c language"
  a+=""
  for(t<-a){
     println(t)
   }
  
  
  val s= ListBuffer[Any]()    // for any datatype
 
  s+=1
  s+="sqoop"
  s+="flume"
  s+=1.255
  s+=10555
  println("To get the Any element")
  var d=Iterator(s)
  while(d.hasNext)
  {
    println(d.next())
  }
   
   println("To get the element: " +s.take(2)) //it used to fetch first two element
}
}