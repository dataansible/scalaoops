package collectionWorkout

object SampleSet {
def main(args: Array[String]): Unit = {
  
  var s=Set(1,2,3,"hello",1) //It access all datatype
  
  var t:Set[String]=Set("scala","java","c","python") //It access string datatype.
  
  var r:Set[Nothing]=Set() //Empty set
  
  println("To Head: "+s.head)
  println("To tail: "+t.tail)
  println("To check the set: "+r.isEmpty)
  
  for(e<-s)
  {
    println(e)
  }
  
}
}