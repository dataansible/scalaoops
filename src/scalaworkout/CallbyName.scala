package scalaworkout

object CallbyName {
def main(args: Array[String]): Unit = {
  var r=new Call
  println("Sub =" +sub(r.show(3)))
  println("Add =" +add(r.show(6)))
  
  
}
def sub(b: => Int) : Int ={
  return b*b
}
def add(c: => Int) : Int ={
  return c+c
}
}
class Call
{
def show(a:Int) : Int ={
  return a
}  
}