package scalaworkout
//Within class have same method name but different argument
object Overload {
def main(args: Array[String]): Unit = {
  var d=new Load
  d.show(5)
  d.show(6, 9)
}
}
class Load
{
def show(a:Int)
{
  println("value of a=" +a)
}
def show(c:Int,b:Int)
{
  println("value of b=" +b)
  println("value of c=" +c)
}
}