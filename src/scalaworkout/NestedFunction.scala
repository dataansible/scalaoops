package scalaworkout
//Function inside the another function is called as nested function
object NestedFunction {
def main(args: Array[String]): Unit = {
  
}
}
class Nested
{
  def max(a:Int, b:Int, c:Int) = {
    def max(x:Int, y:Int) = 
      {
      if (x>y) x else y
      }
    max(a,max(b,c))
  }
}
  
