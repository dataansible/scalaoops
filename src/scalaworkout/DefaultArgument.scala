package scalaworkout

object DefaultArgument {
def main(args: Array[String]): Unit = {
  val k=new Default1
  k.show(a=6, b=3) //we can change the default value also
}
}
class Default
{
def show(a:Int=5,b:Int=8)  
{
println("sum of values= "+(a+b))  
}
}
//method override is possible
class Default1 extends Default
{
 override def show(a:Int=5,b:Int=8)  
{
println("sum of values= "+(a+b))  
} 
}