package scalaworkout

object MultipleInheritance {
def main(args: Array[String]): Unit = {
  val f=new Son1
  f.show()
  f.method()
  f.show1()
}
}
class GrandFather
{
  def show()
  {
   println("GrandFather house")
  }
}
class Father1 extends GrandFather
{
  def method()
  {
    println("Father car")
  }
}

class Son1 extends Father1
{
  def show1()
  {
    println("My bike")
  }
}