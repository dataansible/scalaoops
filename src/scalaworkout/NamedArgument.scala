package scalaworkout
//overload,override is possible
object NamedArgument {
def main(args: Array[String]): Unit = {
  val y=new Named
  y.show(b="Scala",a=23)
}
}
class Named
{
  def show(a:Int,b:String)
  {
    println("value of a= "+a)
    println("value of b= "+b)
  }
  
}