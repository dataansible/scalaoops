package scalaworkout

object Abstraction {
def main(args: Array[String]): Unit = {
  var f=new Abstract
  f.node()
  f.show("Hai")
  f.method(5, 9)
}
}
abstract class Abs
{
def show(s:String) //Abstract method
def node()        
def method(a:Int,b:Int)
{
 println("sum= " +(a+b))
}
}

class Abstract extends Abs
{
  def show(s:String)
  {
  println(s)  
    
  }
  def node()
  {
   println("Abstract method") 
  }
}