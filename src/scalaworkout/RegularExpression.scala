package scalaworkout
import scala.util.matching.Regex
object RegularExpression {
def main(args: Array[String]): Unit = {
  val pattern="(s|S)cala".r
  val str="Scala is scalable language and Scala is cool"
  println("To find the first pattern")
  println(pattern findFirstIn str)// It will find first word
  
  println("To find the all pattern matches")
  println((pattern findAllIn str).mkString(","))
  
  println("To replace the pattern matches")
  println(pattern replaceFirstIn(str,"java"))
  
}
}