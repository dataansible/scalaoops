package scalaworkout
//Inheritance means one can enjoy the property of other class along with his own property

object SingleInheritance {
def main(args: Array[String]): Unit = {
  var d=new Son
  d.test()
  d.show()
}
}

class Father
{
  def show()
  {
    println("Father house")
  }
}

class Son extends Father
{
  def test()
  {
   println("My bike") 
  }
}