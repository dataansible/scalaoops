package scalaworkout
import java.io.IOException
object FinallyKeyword {
def main(args: Array[String]): Unit = {
  
  var s=new Array[Int](2)
  
  try{
    s(0)=1
    s(1)=2
    s(2)=3
    s(3)=4
    for(a<-s)
    {
      println(a)
    }
  }
  catch{
    case ex:ArrayIndexOutOfBoundsException =>println ("Index out of bound ")
  }
  finally
  {
    println("Successfully executed...")
  }
}
}