package scalaworkout

object ArrayTest {
def main(args: Array[String]): Unit = {
  
  var a=new Array[Int](4)
  a(0)=8
  a(1)=9
  a(2)=6
  a(3)=4
  //a(4)=5 It show array index out of bound
  for(s<-a)
  {
    println(s)
  }
   var d=Array(1,2,23,26,"haiii")
    for(f<-d)
    {
      println(f)
    }
}
}