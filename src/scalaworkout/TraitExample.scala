package scalaworkout

object TraitExample {
def main(args: Array[String]): Unit = {
  val t=new Exam1
  t.show()
  t.method()
}
}
trait t1
{
def show()
def method()
{
println("It allow non abstract method")  
}
}
trait t2
{
def show()
/*def method()
{
println("haiii")  
}*/
}

class Exam1 extends t1 with t2
{
  def show()
  {
    
    println("welcome to scala")
  }
  override def method()
  {
    super.method()
    println("haiii")
  }
}