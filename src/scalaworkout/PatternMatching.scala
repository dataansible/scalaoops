package scalaworkout

object PatternMatching {
def main(args: Array[String]): Unit = {
  println(test(2))
  println(test(5))
}
def test(x:Int): String=x match{
  case 1 =>"one"
  case 2 =>"two"
  case _ =>"many"
}
}