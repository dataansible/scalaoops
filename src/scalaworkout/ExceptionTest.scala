package scalaworkout
import java.io.IOException
object ExceptionTest {
def main(args: Array[String]): Unit = {
  var a:Int=20
  var b:Int=0
  try
  {
    println(a/b)
  }
  catch
  {
    case ex:ArithmeticException =>println("Not divide by zero")
    case ex:IOException =>println("ExceptionHandling")
  }
}
}