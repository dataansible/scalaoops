package scalaworkout

object Variables {
def main(args: Array[String]): Unit = {
  var a:Int=10  //It is mutable value
  val b:Int=23  //It is immutable value
  a=25
  // b=23 
  var (c:Int,d:String)= (2,"scala") // We have pass multiple values.
  println(a)
  println(b)
  println(c)
  println(d)
}
}