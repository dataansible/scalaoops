package scalaworkout
//super and sub class have same method name and same argument
object Override {
def main(args: Array[String]): Unit = {
  val d=new B
  d.show("Scala")
}
}

class A
{
def show(a:String)
{
println("value of a=" +a)  
}
}
class B extends A
{
override def show(b:String) 
{
  println("value of b=" +b)  
  }
}