package scalaFunction
// we can specify the default value for the function
object DefaultArgument {
def main(args: Array[String]): Unit = {
  val g=new Default
  g.show(a=6, b=8) // we also assign a new value
}
}

class Default
{
def show(a:Int=10,b:Int=5)
{
var s=a+b
println(s)
}
}