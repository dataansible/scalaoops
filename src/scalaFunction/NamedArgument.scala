package scalaFunction
// we can pass the argument to function in different order
object NamedArgument {
  def main(args: Array[String]): Unit = {
   var w=new Named1
   w.show(b="hello", a=5)
  }

}
class Named1
{
def show(a:Int,b:String)
{
println(a)
println(b)
}
}