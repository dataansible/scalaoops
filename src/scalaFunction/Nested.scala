package scalaFunction

object Nested {
def main(args: Array[String]): Unit = {
  println("Max of 5,7,9 = " + max(5,7,9));
  }
 
  def max(a:Int, b:Int, c:Int) = {
    def max(x:Int, y:Int) = 
      {
      if (x>y) x else y
      }
    max(a,max(b,c))
  }
}