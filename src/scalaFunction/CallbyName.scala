package scalaFunction
//we can pass the function as argument to another function
object CallbyName {
def main(args: Array[String]): Unit = {
  val k=new Call
  k.add(k.show(5))
  k.mul(k.show(4))
}
}
class Call
{
def add(x: => Int)
{
println(x+x)  
}
def mul(y: =>Int)
{
println(y*y)  
}
def show(a:Int): Int ={
  return a
}
}