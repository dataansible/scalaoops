package scalaCollection

object IteratorExample1 {
def main(args: Array[String]): Unit = {
  var e=new Iterator1
  e.show()
}
}

class Iterator1{
  
  def show(){
    var a:List[String]=List("java","bigdata","dotnet","sqoop","c++")
    var b=List() //If you not  mention data type it will access 'any'
    println("To sort: "+a.sorted) // according to ascending order
    println("To sortBy: "+a.sortBy { _.length()})// It will sorted according the length
    println("To sortwith: "+a.sortWith(_<_))
    println("To take particular: "+a.take(2))
    println("Last element: "+a.last)
    println("To find the index: "+a.indexOf("sqoop"))
    println("To filter: " +a.filter { _.length > 5})
    println("To fetch the element: "+a.lift(3))
    println("From iterator")
    var f=Iterator(a) 
    while(f.hasNext){
      println(f.next())
    }
  }
}