package scalaCollection

object TuplesExample {
def main(args: Array[String]): Unit = {
  var d=new Tuple
  d.show()
}
}

class Tuple{
  
  def show(){
    val a=(4,2,6,9,"Sriram","hiii",4)
    println(a._3) //to select the particular values
    println(a._5)
    println(a._1)
    println(a._7)
    println()
    a.productIterator.foreach(println) // to fetch all the values
    
  }
}

//In tuple we can't give single value (It is immutable)