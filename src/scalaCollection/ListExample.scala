package scalaCollection

object ListExample {
def main(args: Array[String]): Unit = {
  var c=new SampleList
  c.show()
}
}
class SampleList{
  def show()
  {
    val tech:List[String]=List("bigdata","hadoop","spark","habse","sqoop","hbase")
    val tech1:List[String]=List()
    var tech2:List[Int]=List(1,3,5,6,9,10)
    tech2=List(2,8,7,6)
    
    println("First tech is " +tech.head) //To list the first element
    println("Remaining is " +tech.tail)  //To list the element except first
    println("To check " +tech1.isEmpty)  //To check the list is empty or not
    println("To reverse " +tech.reverse) //To reverse the list
    println("To check the length " +tech.length) //To check the length
    println("To fetch the max " +tech.max) //To find the last element
    println("To fetch the min " +tech.min) //To find the first element
    println("First is " +tech2.head)
    println("<-----Detail----->")
    //tech.productIterator.foreach(println)
    /*for(a<-tech)
    {
      println(a)
    }*/
    println("Using iterator")
    var r=Iterator(tech)
    while(r.hasNext)
    {
      println(r.next())
    }
    
   
  }
  
}