package scalaCollection

object WithOption {
def main(args: Array[String]): Unit = {
  var detail=Map(1->"India",2->"USA",3->"Japan",4->"Russia")
    println("Country=" + display(detail.get(1)))
    println("Country=" + display(detail.get(5)))
  }
  def display(x:Option[String]) =x match { 
    case Some(s) => s
      case None => "?"
  }
}
