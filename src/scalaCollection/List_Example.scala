package scalaCollection
import scala.collection.mutable.ListBuffer
object List_Example {
def main(args: Array[String]): Unit = {
  
  var adamfriends =List("Nick","Bill")
 
  val davidfriends =List("Beck","Kenny","Bill")
  val friendsOffriends=List(adamfriends,davidfriends)
  val uniquefriendsOffriends=friendsOffriends.flatten.distinct
  val uniquefriendsOffriends1=friendsOffriends.flatten //List(Nick, Bill, Beck, Kenny, Bill)
  val uniquefriendsOffriends2=friendsOffriends.distinct //List(List(Nick, Bill), List(Beck, Kenny, Bill))
  
  println(uniquefriendsOffriends)
   println(uniquefriendsOffriends1)
    println(uniquefriendsOffriends2)
    
    var fruits =new ListBuffer[String]() //list support for db connectivity
    fruits += "Apple"
    fruits += "Banana"
    fruits += "Orange"
    val fruitsList =fruits.toList
   println(fruitsList)
  
  val veg=List.fill(3,2)("onion") //3 list and each one list have 2 element
 println(veg)
  /*val nos =List.range(1, 11)
  println(nos)
  val nos1 =List.range(1,11,4)
  println(nos1)
    */
}
}