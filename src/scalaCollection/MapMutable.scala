package scalaCollection
import scala.collection.mutable.Map
object MapMutable {
def main(args: Array[String]): Unit = {
  var e=new MapTest1()
  e.show()
}
}

class MapTest1{
  def show(){
    var b=Map[Int,String]()
    b+=1->"apple"
    b+=2->"graphes"
    b+=3->"orange"
    b+=4->"pineapple"
    b+=5->"apple"  //different key but same values 
    b+=2->"berry"  //same key but different values 
    
    println("To get the id " +b.keys)
    println("To get the id " +b.values)
    println("To check  " +b.isEmpty)
    
    for(d<-b){
      println(d)
    }
  }
}