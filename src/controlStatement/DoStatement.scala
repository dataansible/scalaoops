package controlStatement

object DoStatement {
def main(args: Array[String]): Unit = {
  var  a=1
  do
  {
    println("value of a: "+a)
    a=a+1
  }
  while(a<6)
}
}