package controlStatement

object IfelseStatement {
def main(args: Array[String]): Unit = {
  var a=2
  var b=4
  if(a>b)
    println("a is greater")
  else println("b is greater")
}
}