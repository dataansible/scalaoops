package controlStatement

object ForStatement {
def main(args: Array[String]): Unit = {
  for(a<-1 to 5 if a<=4)
  {
    println(a)
  }
}
}