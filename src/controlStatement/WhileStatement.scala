package controlStatement

object WhileStatement {
def main(args: Array[String]): Unit = {
  var a=1
  while(a<5)
  {
    println("value of a"+a)
    a=a+1
  }
}
}