package scalaString

object String_function {
def main(args: Array[String]): Unit = {
  var a="data"
  var b="dotz"
  var c="datadotz"
  var test="DATADOTZ"
  println("To display Concat:")
  println("string is " +a+b)
  
  println("To display Array to string:")
  var d=Array("a","b","c") //Array to string transformations
  println(d.mkString)
  println(d.mkString(","))
  println(d.mkString("(", ",", ")"))
  
  println("To get the string position:")
  println(a.charAt(0))// get a character at a String position
  println(a.charAt(1))
  
  println("To check the string equal:")
  println(a.equals(b))//To check the two string equal or not
  println(a.equals(c))
  
  println("To display interpolator:")
  println(s"Hello,$b") // interpolation
  
  println("To get length of string:")
  var e=a.length() //to find the length of string
  println("length of string=" +e)
  
  println("To replace:")
  println(a.replace("a", "i"))   //replace by another character
  println(a.replaceAll("data", "dd"))
  println(a.replaceFirst("z","s"))
  
  println("To get substring:")
  println(c.substring(0,4)) 
  println(c.substring(1,c.length-1))
  
  println("To convert upper,lower,capitalize")
  println(test.toLowerCase())
  println(a.toUpperCase())
  println(a.capitalize)
  
  println("To use distinct,intersect,different")
  println(a.distinct)
  println(a.intersect(b))
  println(a.diff(b))
  
  println("To display one character per line")
  (a.foreach { println })
  
  println("To reverse the string")
  println(a.reverse)
  
 /* println("To format :")
  println(a.format(b))*/
  
}
}