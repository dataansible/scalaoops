package scalaString

object String_builder {
  def main(args: Array[String]): Unit = {
    
    println("To concat the string")
    var sb1=new StringBuilder("data")
    println(sb1.append("dotz"))
    
    println("To insert the string")
    var sb2=new StringBuilder("data")
    println(sb2.insert(0,"hello"))
    
    println("To replace the string")
    var sb3=new StringBuilder("data")
    println(sb3.replace(0, 1, "hello"))
    
    println("To delete the string")
    var sb4=new StringBuilder("datadotz")
    println(sb4.delete(0, 4))
    
    println("To reverse the string")
    var sb5=new StringBuilder("datadotz")
    println(sb5.reverse)
    
    
    
    
  }

}