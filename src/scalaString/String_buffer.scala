package scalaString

object String_buffer {
  def main(args: Array[String]): Unit = {
  var sb=new StringBuffer("scala")
  println("To append the string")
  sb.append("program")
  println(sb)
  
  println("To insert the string to particular position")
  var sb1=new StringBuffer("scala")
  sb1.insert(0,"program")
  println(sb1)
  
  println("To replaces the given string from the specified beginIndex and endIndex.")
  var sb2=new StringBuffer("scala")
  sb2.replace(1, 3, "program")
  println(sb2)
  
  println("To delete the given string from the specified beginIndex and endIndex.")
  var sb3=new StringBuffer("scala")
  sb3.delete(0, 2)
  println(sb3)
  
  println("To reverse the current string")
  var sb4=new StringBuffer("scala")
  sb4.reverse()
  println(sb4)
  }

}