package scalaArray

object Sample {
  def main(args: Array[String]): Unit = {
   /* var myList = Array(1.9, 2.9, 3.4, 3.5,2.2,36,6.3,8.9,7.5,4.5,41.2,6.6,98.0)
    for(x<-myList)
      println(x)
  }
*/
    var z = new Array[String](3)
    //var myarray:Array[Int]=new Array[Int](5)
    z(0)="java"
    z(1)="scala"
    z(2)="program"
    //z(3)="hello"
    for(a<-z)
    {
      println(a)
    }
}
}