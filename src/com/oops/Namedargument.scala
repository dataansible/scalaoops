package com.oops
//it used to pass the different value for the method
//It equal to overload
object Namedargument {
def main(args: Array[String]): Unit = {  //check overload
  /*named1(2,3)*/
  named1(b=15,a=5)
  named1(5)
  /*var c=new Named2            //method override
  c.named1(b=25,a=10)*/
}
def named1(a:Int,b:Int)
{
println("a values=" +a) 
println("b values=" +b) 
}
def named1(a:Int)
{
  println("a values=" +a)
}
}
//check for override
/*class Named1 
{
def named1(a:Int,b:Int)
{
println("a values=" +a) 
println("b values=" +b) 
}  
}
class Named2 extends Named1
{
 override def named1(a:Int,b:Int)
{
println("a values=" +a) 
println("b values=" +b)
println("method override")
} 
}*/