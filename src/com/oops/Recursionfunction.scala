package com.oops

object Recursionfunction {
  def main(args: Array[String]): Unit = {
   for(i <-1 to 3){        // recursion using for loop
     println("factorial of " +i+":= " +factorial(i))
   }
   def factorial(a:Int): Int = {
     if (a <= 1)
     1
     else
       a * factorial(a-1)
   }
  }
}