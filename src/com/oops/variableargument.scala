package com.oops

object variableargument {
   def main(args: Array[String]) {
      printAll("sriram",12,35,95,36); //type mismatch
      
 }
   
   def printAll(b:String, a:Int*) = {
     for(r<-a) 
     {
       println(r)
     }
     
     /*var f=new Vari
     f.show(12,23,25,63)*/   // method override
     
   }
}

// method override
/*class Variable{
  def show(a:Int*)
  {
    for(d<-a)
    {
      println(d)
    }
  }
}
class Vari extends Variable
{
  override def show(a:Int*)
  {
    for(d<-a)
    {
      println(d)
    }
  }
  
}*/