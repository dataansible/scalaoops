package com.oops

object Case {
def main(args: Array[String]): Unit = {
  var d=new D
  d.disp
  d.show
}
}

abstract class Sea
{
def show
}

trait Water
{
def disp  
}

class D extends Sea with Water
{
  def show
  {
    println("Abstract")
    
  }
  def disp
  {
    println("interface")
  }
}