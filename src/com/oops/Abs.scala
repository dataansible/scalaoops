package com.oops

object Abs {
  def main(args: Array[String]): Unit = {
    val a=new Sai()
    a.non();
    a.show();
    a.print();
  }

}

abstract class Sam
{
def show()
{
println("non abstract")  
}
def print{}
}

class Sai extends Sam
{
def non()
{
println("normal class")  
}
override def print()
{
  println("abstract method")
}
}