package com.oops

object Level {
def main(args: Array[String]): Unit = {
  val d=new C
  d.size()
  d.volume()
  d.velocity()
}
}

class A
{
def size()
{
  println("A")
}
}

class B extends A
{
def volume()
{
println("B")  
}
}

class C extends B
{
def velocity()
{
println("c")  
}
}