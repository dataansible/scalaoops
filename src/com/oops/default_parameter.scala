package com.oops

object default_parameter {
def main(args: Array[String]): Unit = {
  /*var d=new Default1     //method override
  var e=d.addInt(a=15,b=6) //we can change the value
  println(e)*/
  println("values:" +show())
}
  def show(a:Int=10,b:Int=12): Int={
    return a+b
  }
  
}

/*class Default{              //method override
def addInt(a:Int=5,b:Int=6): Int={
  return a+b
}
}

class Default1 extends Default{
 override def addInt(a:Int=5,b:Int=6): Int={
  return a+b
}
}*/
