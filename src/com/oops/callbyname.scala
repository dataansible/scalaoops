package com.oops

object callbyname {
def main(args: Array[String]): Unit = {
  sub(add())
  sub(mul)
}
def add(): Int ={
  println("code...")
  return 10+2
}
def mul(): Int ={
  println("code...")
  return 36*9
}
def sub(s: => Int) ={
  println("test")
  println("values="+s)
}
}