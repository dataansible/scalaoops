package sampleCollection

object Test_Set {
def main(args: Array[String]): Unit = {
  val k=new Set1
  k.show()
}
}
class Set1
{
  def show()
  {
    var g: Set[String]=Set("hadoop","Scala","spark","flume","kafka")
    println("To check the element :" +g.canEqual("flume"))
    println("To use mkstring we separate: " +g.mkString(","))
    println("To check the hashcode of set: " +g.hashCode())
  }
}