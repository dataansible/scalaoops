package fileIO
import java.util.Scanner
import java.io.IOException
import java.io._
object FileExample {
def main(args: Array[String]): Unit = {
  val h=new Example
  h.show
}
}
class Example
{
def show{
  
  var scanner=new Scanner(System.in)
    
    
    print("Enter the name: " )
    val a:String=scanner.nextLine()
    
    print("Enter the age: ")
    val b=scanner.nextInt()
    val k=b.toString()
    
    print("Enter the phoneno:")
    val c=scanner.nextLong()
    val t=c.toString()
    scanner.nextLine()
    
    print("Enter the emailID:")
    val d:String=scanner.nextLine()
  try{
    
  //val test=new FileWriter("/home/sriram/Desktop/detail.txt",true)
    
    //Printwriter only for append
  //val test = new PrintWriter(new FileOutputStream(new File("Test.txt"),true)))
    
  //For new use bufferedwriter and append
  val test=new BufferedWriter(new FileWriter("/home/sriram/Desktop/detail.txt",true))
  test.write(a)
  test.newLine()
  test.write(k)
  test.newLine()
  test.write(t)
  test.newLine()
  test.write(d)
  test.newLine()
  test.close()
 
    }
   
  catch{
    case ex:IOException =>println("File path not found")
  }
  finally{
  println("Successfully saved...")
  }
    }
}  
