package collection

object ListTest {
def main(args: Array[String]): Unit = {
  var a:List[String]=List("java","scala","sqoop","sql")
  for(f<-a)
  {
    println(f)
  }
  println("Head of list: "+a.head)
  println("Tail of list: "+a.tail)
  println("Empty or not: "+a.isEmpty)
}
}