package stringWorkout

object Stringbuffer {
def main(args: Array[String]): Unit = {
  var a=new StringBuffer("scala")
  println("To append:")
  a.append("programming")
  println(a)
  
  println("To reverse")
  var b=new StringBuffer("Java")
  b.reverse()
  println(b)
  
  println("To replace")
  var c=new StringBuffer("Hadoop")
  c.replace(1, 2, "bigdata")
  println(c)
  
  println("To Insert")
  var d=new StringBuffer("Hadop")
  d.insert(4, "o")
  println(d)
}
}