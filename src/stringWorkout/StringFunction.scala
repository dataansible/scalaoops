package stringWorkout

object StringFunction {
def main(args: Array[String]): Unit = {
  var a="data"
  var b="dotz"
  var c="datadotz"
  var test="DATADOTZ"
  println("To use distinct,intersect,different")
  println(a.distinct)
  println(a.intersect(b))
  println(a.diff(b))
  
  println("To convert upper,lower,capitalize")
  println(test.toLowerCase())
  println(a.toUpperCase())
  println(a.capitalize)
  
  println("To display one character per line")
  (a.foreach { println })
  
  println("To display interpolator:")
  println(s"Hello,$b") // interpolation
}
}