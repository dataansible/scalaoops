package stringWorkout

object Currying {
def main(args: Array[String]): Unit = {
  val a:String="hello"
  val b:String="scala"
  val c:String="programming"
  str(a)(b)(c)
}
def str(a:String)(b:String)(c:String)
{
  val d=a+b+c
  println(d)
}
}