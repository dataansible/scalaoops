package stringWorkout

object Sample {
  var a:String="hello"
  var b="world"
  def main(args: Array[String]): Unit = {
    //a=a.concat(b)
    a.concat(b)//Immutable
    println(a)
    println(a.toUpperCase())
    println(a.contains("h"))
   
  }

}