package stringWorkout

object Stringbuilder {
def main(args: Array[String]): Unit = {
  println("To concat the string")
    var a=new StringBuilder("data")
    println(a.append("dotz"))
    
    println("To insert the string")
    var b=new StringBuilder("data")
    println(b.insert(0,"hello"))
    
    println("To replace the string")
    var c=new StringBuilder("data")
    println(c.replace(0, 1, "hello"))
    
    println("To delete the string")
    var d=new StringBuilder("datadotz")
    println(d.delete(0, 4))
    
    println("To reverse the string")
    var e=new StringBuilder("datadotz")
    println(e.reverse)
}
}