package exceptionHandling
import java.io.FileNotFoundException
import java.io.IOException

object DemoFinally extends App{
var d=new FinallyExam
d.show()
}

class FinallyExam{
  def show(){
    val a:Int=5
    val b:Int=0
    try{
    var d=a/b
    println(d)
    }
    catch {
      case ex: ArithmeticException=>{
        println("Arithmetic Exception found...")
      }
      case ex: IOException => {
            println("IO Exception")
         }
    }
    finally{
      println("Finally exiting...")
    }
  }
}