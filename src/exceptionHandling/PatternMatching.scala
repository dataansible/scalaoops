package exceptionHandling

object PatternMatching {
def main(args: Array[String]): Unit = {
  var s=new Pattern
  println(s.show(1))
  println(s.show(2))
  println(s.show(500))
}
}

class Pattern{
  def show(x:Any):Any = x match{
    case 1=>"one"
    case 2=>"two"
    case 3=>"three"
    case _=>"many"
  }
}