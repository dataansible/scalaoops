package exceptionHandling
import java.io.IOException

object Exceptiontest {
def main(args: Array[String]): Unit = {
  var s=new ExceptionExam
  s.show()
}
}

class ExceptionExam{
  def show(){
    val a:Int=5
    val b:Int=0
    try{
    var d=a/b
    println(d)
    }
    catch {
      case ex: ArithmeticException=>{
        println("Arithmetic Exception found...")
      }
      case ex: IOException => {
            println("IO Exception")
         }
    }
  }
}